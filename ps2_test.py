#!/usr/bin/python

import math
import random
import unittest

import ps2

class RectangularRoomTest(unittest.TestCase):

    def testCleanTileAtPosition(self):
        #given:
        room = ps2.RectangularRoom(4, 4)
        tilePosition = ps2.Position(2, 3)
        self.assertFalse(room.isTileCleaned(tilePosition.getX(), tilePosition.getY()))

        #when:
        room.cleanTileAtPosition(tilePosition)
        self.assertTrue(room.isTileCleaned(tilePosition.getX(), tilePosition.getY()))

    def testCleanTileAtFloatPosition(self):
        #given:
        room = ps2.RectangularRoom(2, 1)
        self.assertFalse(room.isTileCleaned(0, 0))

        #when:
        room.cleanTileAtPosition(ps2.Position(0.6, 0.3))
        self.assertTrue(room.isTileCleaned(0, 0))
        
    def testNumTiles(self):
        #given:
        room = ps2.RectangularRoom(3, 4)

        #expect:
        self.assertEquals(12, room.getNumTiles())

    def testIsPositionInRoom(self):
        #given:
        room = ps2.RectangularRoom(10, 12)

        #and:
        testCoordinates = [(1, 3), (4, 6), (9, 11)]
        testPositions = [ps2.Position(x, y) for x, y in testCoordinates]

        #and:
        invalidCoordinates = [(-2, -5), (-1, -1), (10, 12), (11, 9), (-0.10, 0.00), (0.00, -0.10), (-0.10, -0.10)]
        invalidPositions = [ps2.Position(x, y) for x, y in invalidCoordinates]

        #and:
        floatCoordinates = [(0.10, 0.00), (0.00, 0.10), (3.07, 3.41)]
        floatPositions = [ps2.Position(x, y) for x, y in floatCoordinates]

        #expect:
        for position in testPositions:
            self.assertTrue(room.isPositionInRoom(position))

        #and:
        for position in invalidPositions:
            self.assertFalse(room.isPositionInRoom(position))

        #and:
        for position in floatPositions:
            self.assertTrue(room.isPositionInRoom(position))

    def testGetRandomPosition(self):
        #given:
        height = 23
        width = 32
        room = ps2.RectangularRoom(width, height)

        #when:
        position1 = room.getRandomPosition()
        coordinates1 = (position1.getX(), position1.getY())
        position2 = room.getRandomPosition()
        coordinates2 = (position2.getX(), position2.getY())

        #then:
        self.assertTrue(coordinates1[0] < width)
        self.assertTrue(coordinates1[1] < height)
        self.assertTrue(coordinates2[0] < width)
        self.assertTrue(coordinates2[1] < height)

        #and:
        self.assertNotEqual(coordinates1, coordinates2)

    def testGetNumCleanedTiles(self):
        #given:
        room = ps2.RectangularRoom(10, 10)
        cleanTiles = [(x, y) for x in range(2) for y in range(2)]
        for coordinates in cleanTiles:
            room.cleanTileAtPosition(ps2.Position(coordinates[0], coordinates[1]))

        #expect:
        self.assertEquals(len(cleanTiles), room.getNumCleanedTiles())
        
class RobotTest(unittest.TestCase):

    def testRobotCleansTileItIsCreatedOn(self):
        #given:
        width = 7
        height = 3
        room = ps2.RectangularRoom(width, height)

        #and:
        random.seed(0)
        angle = random.randrange(360)
        position = room.getRandomPosition()

        #when:
        random.seed(0)
        speed = 2.3
        robot = ps2.Robot(room, speed)

        #then:
        actualPosition = robot.getRobotPosition()
        self.assertEqual(position.getX(), actualPosition.getX())
        self.assertEqual(position.getY(), actualPosition.getY())

        #and:
        self.assertTrue(room.isTileCleaned(position.getX(), position.getY()))

        #and:
        self.assertEqual(2.3, robot.getRobotSpeed())
        self.assertEquals(angle, robot.getRobotDirection())

    def testSettingCorrectRobotPosition(self):
        #given:
        room = ps2.RectangularRoom(5, 5)
        robot = ps2.Robot(room, 1.0)

        #when:
        robot.setRobotPosition(ps2.Position(3, 2))
        robot.setRobotPosition(ps2.Position(6, 7))

        #then:
        position = robot.getRobotPosition()
        self.assertEquals(3, position.getX())
        self.assertEquals(2, position.getY())

class StandardRobotTest(unittest.TestCase):

    def testUpdatePositionAndClean(self):
        #given:
        room = ps2.RectangularRoom(10, 10)
        robot = ps2.StandardRobot(room, 1.0)

        #and:
        start = ps2.Position(5, 5)
        robot.setRobotPosition(start)
        robot.setRobotDirection(360)

        #and:
        self.assertEquals(1, room.getNumCleanedTiles())

        #when:
        robot.updatePositionAndClean()
        end = robot.getRobotPosition()

        #then:
        self.assertEquals(5.0, end.getX())
        self.assertEquals(6.0, end.getY())
        self.assertEquals(2, room.getNumCleanedTiles())

    def testUpdatePositionAndCleanOutOfBoundary(self):
        #given:
        room = ps2.RectangularRoom(4, 4)
        robot = ps2.StandardRobot(room, 1.0)
        self.assertEquals(1, room.getNumCleanedTiles())

        #and:
        start = ps2.Position(3, 3)
        robot.setRobotPosition(start)
        robot.setRobotDirection(360)

        #and:
        random.seed(0)
        nextDirection = random.randrange(360)
        
        #when:
        random.seed(0)
        robot.updatePositionAndClean()
        end = robot.getRobotPosition()

        #then:
        self.assertEquals(nextDirection, robot.getRobotDirection())
        self.assertEquals(start.getX(), end.getX())
        self.assertEquals(start.getY(), end.getY())

class RandomWalkRobotTest(unittest.TestCase):

    def testUpdatePositionAndClean(self):
        #given:
        room = ps2.RectangularRoom(10, 10)
        robot = ps2.RandomWalkRobot(room, 1.0)
        startPosition = ps2.Position(5, 5)
        robot.setRobotPosition(startPosition)

        #and:
        startDirection = 360
        robot.setRobotDirection(startDirection)

        #and:
        self.assertEquals(1, room.getNumCleanedTiles())

        #and:
        random.seed(0)
        expectedDirection = random.randrange(360)

        #when:
        random.seed(0)
        robot.updatePositionAndClean()
        endPosition = robot.getRobotPosition()

        #then:
        self.assertEquals(2, room.getNumCleanedTiles())

        #and:
        self.assertEquals(expectedDirection, robot.getRobotDirection())
        self.assertTrue(math.fabs(startPosition.getX() - endPosition.getX()) < 0.1)
        self.assertEquals(startPosition.getY() + 1, endPosition.getY())

    def testUpdatePositionAndCleanOutOfBoundary(self):
        #given:
        room = ps2.RectangularRoom(7, 7)
        robot = ps2.RandomWalkRobot(room, 1.0)

        #and:
        startPosition = ps2.Position(6, 6)
        robot.setRobotPosition(startPosition)
        robot.setRobotDirection(360)

        #and:
        random.seed(0)
        expectedDirection = random.randrange(360)

        #when:
        random.seed(0)
        robot.updatePositionAndClean()
        endPosition = robot.getRobotPosition()
        endDirection = robot.getRobotDirection()

        #then:
        self.assertEquals(1, room.getNumCleanedTiles())
        self.assertEquals(startPosition.getX(), endPosition.getX())
        self.assertEquals(startPosition.getY(), endPosition.getY())
        self.assertEquals(expectedDirection, robot.getRobotDirection())
    
if __name__ == '__main__':
    unittest.main()
